<?php
session_start();
include_once 'db.php';
$data=$_POST['data'];
$data = json_decode(stripslashes($_POST['data']),true);
foreach($data as $d){
    $lname=$d['last'];
    $fname=$d['first'];
    $email=$d['email'];
    $classid=$_SESSION['classid'];
    $records = $databaseConnection->prepare('INSERT INTO  students (LastName,FirstName,Email,profile,classid)
    VALUES (:LastName,:FirstName,:Email,:profile,:classid)');
    $records->bindParam(':LastName',$lname);
    $records->bindParam(':FirstName',$fname);
    $records->bindParam(':Email',$email);
    $url=addcslashes('/studentimg/dimg.png',"/");
    $records->bindParam(':profile',$url);
    $records->bindParam((':classid'),$classid);
    $records->execute();
}
echo 'success';