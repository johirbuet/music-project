<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>UML | Add Students</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Icon Image -->
    <link rel="icon" href="images/UML.jpg">

    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">


        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Add Student (s)</h3>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel" style="height:600px;">
                            <div>
                                <table class="authors-list">
                                    <tr>
                                        <td>First Name</td><td>Last Name</td><td>Email</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input placeholder="Enter first Name" type="text" id="first_name" name="first_name"/>
                                        </td>
                                        <td>
                                            <input placeholder="Enter last Name" type="text" id="last_name" name="last_name" />
                                        </td>
                                        <td>
                                            <input placeholder="Enter email" type="text" id="email" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <row>
                                <div class="col-md-6 col-sm-6 col-xs-6" style="margin-top: 10px;">
                                    <a href="#" title="" class="add-author">Add another student</a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6" style="margin-top: 10px;">
                                    <button type="button" id="stsubmit" class="btn btn-info btn-xs">Submit</button>
                                </div>
                            </row>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- validator -->
<script src="../vendors/validator/validator.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="js/custom.js"></script>

<!-- Add Student row -->
<script>
    jQuery(function(){
        var counter = 1;
        jQuery('a.add-author').click(function(event){
            event.preventDefault();
            counter++;
            var newRow = jQuery('<tr><td><input placeholder="Enter first Name" type="text" id="first_name' +
                counter + '"/></td><td><input placeholder="Enter last Name" type="text" id="last_name' +
                counter + '"/></td><td><input placeholder="Enter email" type="text" id="email' +
                counter + '"/></td></tr>');
            jQuery('table.authors-list').append(newRow);
        });
        $("#stsubmit").on('click',function(){
            var data=[];
            var first=$("#first_name").val();
            var last=$("#last_name").val();
            var email=$("#email").val();
            var std={"first":first,"last":last,"email":email};
            data.push(std);
            for(i=2;i<=counter;i++)
            {
                var fn=$("#first_name"+i).val();
                var ln=$("#last_name"+i).val();
                var em=$("#email"+i).val();
                var s={"first":fn,"last":ln,"email":em};
                data.push(s);
            }
            var jsonString = JSON.stringify(data);
            $.ajax({
                url: 'tryaddstudent.php',
                data: {'data':jsonString},
                type: 'POST',
                error: function() {
                    console.log('Error');
                },
                success: function(msg) {
                    console.log(msg);
                    //window.location.href = "standings";
                }
            });
        });
    });
</script>

<!-- validator -->
<script>
    // initialize the validator function
    validator.message.date = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
    });

    $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();

        return false;
    });
</script>
<!-- /validator -->

</body>
</html>