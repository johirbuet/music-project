<?php
/**
 * Created by PhpStorm.
 * User: johir
 * Date: 6/11/2016
 * Time: 4:38 PM
 */

session_start();

include_once "db.php";

$data=$_POST['data'];
$fname=$data['fname'];
$lname=$data['lname'];
$email=$data['email'];
$pwd=md5($data['pwd']);
$inst=$data['inst'];
$stdsize=$data['stdsize'];

$records = $databaseConnection->prepare('INSERT INTO  instructor (LastName,FirstName,Email,Password,Institution,StudioSize)
  VALUES (:LastName,:FirstName,:Email,:Password,:Institution,:StudioSize)');
$records->bindParam(':LastName',$lname);
$records->bindParam(':FirstName',$fname);
$records->bindParam(':Email',$email);
$records->bindParam(':Password',$pwd);
$records->bindParam(':Institution',$inst);
$records->bindParam(':StudioSize',$stdsize);
$records->execute();


