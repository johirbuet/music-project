<?php
session_start();
include "db.php";

$data=$_POST['data'];
$email=$data['email'];
$pwd=$data['pwd'];
$sql="SELECT * FROM  students WHERE Email = :email";
$records = $databaseConnection->prepare($sql);
$records->bindParam(':email',$email);
$records->execute();
$results = $records->fetch(PDO::FETCH_ASSOC);
if(count($results) > 0 && $pwd==$results['password']){
    $_SESSION['student'] = $results['Email'];
    $_SESSION['fname']=$results['FirstName'];
    $_SESSION['lname']=$results['LastName'];
    $_SESSION['id']=$results['StudentID'];
    $_SESSION['profile']=$results['profile'];
    $_SESSION['address']=$results['address'];
    $_SESSION['birthday']=$results['birthday'];
    $_SESSION['hometown']=$results['hometown'];
    $_SESSION['classid']=$results['classid'];
    $_SESSION['school']=$results['school'];
    $_SESSION['nickname']=$results['nickname'];
    $_SESSION['name']=$results['FirstName'].' '.$results['LastName'];

    echo 'success';
}else{

    echo 'error';

}