<?php

/**

 * Created by PhpStorm.

 * User: johir

 * Date: 5/20/2016

 * Time: 11:30 PM

 */

session_start();

include_once "db.php";

$data=$_POST['data'];

$judgeid=$_SESSION['judgeid'];

$studentid=20015;

$techinique=$data['techinique'];

$rhythmic=$data['rhythmic'];

$tempo=$data['tempo'];

$dynamics=$data['dynamics'];

$articulation=$data['articulation'];

$inotation=$data['inotation'];

$tone=$data['tone'];

$style=$data['style'];

$expression=$data['expression'];

$stagepresence=$data['stagepresence'];

$penalty=$data['penalty'];

$records = $databaseConnection->prepare('INSERT INTO  Score (StudentID,JudgeID,Technique,Rhythmic,Tempo,Dynamics,Articulation,Intonation,Tone,Style,Expression,StagePresence,Penalty)

VALUES (:studentid,:judgeid,:technique,:rhythmic,:tempo,:dynamics,:articulation,:intonation,:tone,:style,:expression,:stagepresence,:penalty)');

$records->bindParam(':studentid',$studentid );

$records->bindParam(':judgeid',$judgeid);

$records->bindParam(':technique',$techinique );

$records->bindParam(':rhythmic',$rhythmic );

$records->bindParam(':tempo',$tempo );

$records->bindParam(':dynamics',$dynamics );

$records->bindParam(':articulation',$articulation );

$records->bindParam(':intonation',$inotation );

$records->bindParam(':tone',$tone );

$records->bindParam(':style',$style );

$records->bindParam(':expression',$expression );

$records->bindParam(':stagepresence',$stagepresence );

$records->bindParam(':penalty',$penalty);

$records->execute();



$studentid2=20016;

$techinique2=$data['techinique2'];

$rhythmic2=$data['rhythmic2'];

$tempo2=$data['tempo2'];

$dynamics2=$data['dynamics2'];

$articulation2=$data['articulation2'];

$inotation2=$data['inotation2'];

$tone2=$data['tone2'];

$style2=$data['style2'];

$expression2=$data['expression2'];

$stagepresence2=$data['stagepresence2'];

$penalty2=$data['penalty2'];

$records2 = $databaseConnection->prepare('INSERT INTO  Score (StudentID,JudgeID,Technique,Rhythmic,Tempo,Dynamics,Articulation,Intonation,Tone,Style,Expression,StagePresence,Penalty)

VALUES (:studentid2,:judgeid,:technique2,:rhythmic2,:tempo2,:dynamics2,:articulation2,:intonation2,:tone2,:style2,:expression2,:stagepresence2,:penalty2)');

$records2->bindParam(':studentid2',$studentid2 );

$records2->bindParam(':judgeid',$judgeid);

$records2->bindParam(':technique2',$techinique2 );

$records2->bindParam(':rhythmic2',$rhythmic2 );

$records2->bindParam(':tempo2',$tempo2 );

$records2->bindParam(':dynamics2',$dynamics2 );

$records2->bindParam(':articulation2',$articulation2 );

$records2->bindParam(':intonation2',$inotation2 );

$records2->bindParam(':tone2',$tone2 );

$records2->bindParam(':style2',$style2 );

$records2->bindParam(':expression2',$expression2 );

$records2->bindParam(':stagepresence2',$stagepresence2 );

$records2->bindParam(':penalty2',$penalty2);

$records2->execute();



$techinique = 0;

$rhythmic = 0;

$tempo = 0;

$dynamics = 0;

$articulation = 0;

$inotation = 0;

$tone = 0;

$style = 0;

$expression = 0;

$stagepresence = 0;

$penalty=0;

$total = 0;

$studentid=20015;

$records = $databaseConnection->prepare('SELECT * FROM Score where StudentID=:studentid');

$records->bindParam(':studentid',$studentid);

$records->execute();

while ($results = $records->fetch(PDO::FETCH_ASSOC)) {

    $techinique += intval($results['Technique']);

    $rhythmic += intval($results['Rhythmic']);

    $tempo += intval($results['Tempo']);

    $dynamics += intval($results['Dynamics']);

    $articulation += intval($results['Articulation']);

    $inotation += intval($results['Intonation']);

    $tone += intval($results['Tone']);

    $style += intval($results['Style']);

    $expression += intval($results['Expression']);

    $stagepresence += intval($results['StagePresence']);

    $penalty+=intval($results['Penalty']);

    $total += 1;

}

if ($total > 0) {

    $techinique = round($techinique / $total);

    $rhythmic = round($rhythmic / $total);

    $tempo = round($tempo / $total);

    $dynamics = round($dynamics / $total);

    $articulation = round($articulation / $total);

    $inotation = round($inotation / $total);

    $tone = round($tone / $total);

    $style = round($style / $total);

    $expression = round($expression / $total);

    $stagepresence = round($stagepresence / $total);

    $penalty = round($penalty / $total);

}

$_SESSION['techinique']=$techinique;

$_SESSION['rhythmic']=$rhythmic;

$_SESSION['tempo']=$tempo;

$_SESSION['dynamics']=$dynamics;

$_SESSION['articulation']=$articulation;

$_SESSION['inotation']=$inotation;

$_SESSION['tone']=$tone;

$_SESSION['style']=$style;

$_SESSION['expression']=$expression;

$_SESSION['stagepresence']=$stagepresence;

$_SESSION['penalty']=$penalty;



$techinique2 = 0;

$rhythmic2 = 0;

$tempo2 = 0;

$dynamics2 = 0;

$articulation2 = 0;

$inotation2 = 0;

$tone2 = 0;

$style2 = 0;

$expression2 = 0;

$stagepresence2 = 0;

$penalty2=0;

$total = 0;

$studentid2=20016;

$records2 = $databaseConnection->prepare('SELECT * FROM Score where StudentID=:studentid2');

$records2->bindParam(':studentid2',$studentid2);

$records2->execute();

while ($results2 = $records2->fetch(PDO::FETCH_ASSOC)) {

    $techinique2 += intval($results2['Technique']);

    $rhythmic2 += intval($results2['Rhythmic']);

    $tempo2 += intval($results2['Tempo']);

    $dynamics2 += intval($results2['Dynamics']);

    $articulation2 += intval($results2['Articulation']);

    $inotation2 += intval($results2['Intonation']);

    $tone2 += intval($results2['Tone']);

    $style2 += intval($results2['Style']);

    $expression2 += intval($results2['Expression']);

    $stagepresence2 += intval($results2['StagePresence']);

    $penalty2+=intval($results2['Penalty']);

    $total += 1;

}

if ($total > 0) {

    $techinique2 = round($techinique2 / $total);

    $rhythmic2 = round($rhythmic2 / $total);

    $tempo2 = round($tempo2 / $total);

    $dynamics2 = round($dynamics2 / $total);

    $articulation2 = round($articulation2 / $total);

    $inotation2 = round($inotation2 / $total);

    $tone2 = round($tone2 / $total);

    $style2 = round($style2 / $total);

    $expression2 = round($expression2 / $total);

    $stagepresence2 = round($stagepresence2 / $total);

    $penalty2 = round($penalty2 / $total);

}



$_SESSION['techinique2']=$techinique2;

$_SESSION['rhythmic2']=$rhythmic2;

$_SESSION['tempo2']=$tempo2;

$_SESSION['dynamics2']=$dynamics2;

$_SESSION['articulation2']=$articulation2;

$_SESSION['inotation2']=$inotation2;

$_SESSION['tone2']=$tone2;

$_SESSION['style2']=$style2;

$_SESSION['expression2']=$expression2;

$_SESSION['stagepresence2']=$stagepresence2;

$_SESSION['penalty2']=$penalty2;

echo 'success';



