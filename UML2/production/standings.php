<?php
session_start();
if(!isset($_SESSION['username']))
{
    header('location:login');
}
include_once "db.php";
$techinique = 0;

$rhythmic = 0;

$tempo = 0;

$dynamics = 0;

$articulation = 0;

$inotation = 0;

$tone = 0;

$style = 0;

$expression = 0;

$stagepresence = 0;

$penalty=0;

$total = 0;

$studentid=20015;

$records = $databaseConnection->prepare('SELECT * FROM Score where StudentID=:studentid');

$records->bindParam(':studentid',$studentid);

$records->execute();

while ($results = $records->fetch(PDO::FETCH_ASSOC)) {

    $techinique += intval($results['Technique']);

    $rhythmic += intval($results['Rhythmic']);

    $tempo += intval($results['Tempo']);

    $dynamics += intval($results['Dynamics']);

    $articulation += intval($results['Articulation']);

    $inotation += intval($results['Intonation']);

    $tone += intval($results['Tone']);

    $style += intval($results['Style']);

    $expression += intval($results['Expression']);

    $stagepresence += intval($results['StagePresence']);

    $penalty+=intval($results['Penalty']);

    $total += 1;

}

if ($total > 0) {

    $techinique = round($techinique / $total);

    $rhythmic = round($rhythmic / $total);

    $tempo = round($tempo / $total);

    $dynamics = round($dynamics / $total);

    $articulation = round($articulation / $total);

    $inotation = round($inotation / $total);

    $tone = round($tone / $total);

    $style = round($style / $total);

    $expression = round($expression / $total);

    $stagepresence = round($stagepresence / $total);

    $penalty = round($penalty / $total);

}
$ova1=100*($techinique + $rhythmic +$tempo +$dynamics + $articulation + $inotation + $tone + $style + $expression + $stagepresence)/200;


$_SESSION['techinique']=$techinique;

$_SESSION['rhythmic']=$rhythmic;

$_SESSION['tempo']=$tempo;

$_SESSION['dynamics']=$dynamics;

$_SESSION['articulation']=$articulation;

$_SESSION['inotation']=$inotation;

$_SESSION['tone']=$tone;

$_SESSION['style']=$style;

$_SESSION['expression']=$expression;

$_SESSION['stagepresence']=$stagepresence;

$_SESSION['penalty']=$penalty;



$techinique2 = 0;

$rhythmic2 = 0;

$tempo2 = 0;

$dynamics2 = 0;

$articulation2 = 0;

$inotation2 = 0;

$tone2 = 0;

$style2 = 0;

$expression2 = 0;

$stagepresence2 = 0;

$penalty2=0;

$total = 0;

$studentid2=20016;

$records2 = $databaseConnection->prepare('SELECT * FROM Score where StudentID=:studentid2');

$records2->bindParam(':studentid2',$studentid2);

$records2->execute();

while ($results2 = $records2->fetch(PDO::FETCH_ASSOC)) {

    $techinique2 += intval($results2['Technique']);

    $rhythmic2 += intval($results2['Rhythmic']);

    $tempo2 += intval($results2['Tempo']);

    $dynamics2 += intval($results2['Dynamics']);

    $articulation2 += intval($results2['Articulation']);

    $inotation2 += intval($results2['Intonation']);

    $tone2 += intval($results2['Tone']);

    $style2 += intval($results2['Style']);

    $expression2 += intval($results2['Expression']);

    $stagepresence2 += intval($results2['StagePresence']);

    $penalty2+=intval($results2['Penalty']);

    $total += 1;

}

if ($total > 0) {

    $techinique2 = round($techinique2 / $total);

    $rhythmic2 = round($rhythmic2 / $total);

    $tempo2 = round($tempo2 / $total);

    $dynamics2 = round($dynamics2 / $total);

    $articulation2 = round($articulation2 / $total);

    $inotation2 = round($inotation2 / $total);

    $tone2 = round($tone2 / $total);

    $style2 = round($style2 / $total);

    $expression2 = round($expression2 / $total);

    $stagepresence2 = round($stagepresence2 / $total);

    $penalty2 = round($penalty2 / $total);

}
$ova2=100*($techinique2 + $rhythmic2 +$tempo2 +$dynamics2 + $articulation2 + $inotation2 + $tone2 + $style2 + $expression2 + $stagepresence2)/200;

$_SESSION['ova1']=$ova1;
$_SESSION['ova2']=$ova2;



$_SESSION['techinique2']=$techinique2;

$_SESSION['rhythmic2']=$rhythmic2;

$_SESSION['tempo2']=$tempo2;

$_SESSION['dynamics2']=$dynamics2;

$_SESSION['articulation2']=$articulation2;

$_SESSION['inotation2']=$inotation2;

$_SESSION['tone2']=$tone2;

$_SESSION['style2']=$style2;

$_SESSION['expression2']=$expression2;

$_SESSION['stagepresence2']=$stagepresence2;

$_SESSION['penalty2']=$penalty2;

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>UML | Standing</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Tab icon -->
    <link rel="icon" href="images/UML.jpg">
      <script src="../vendors/jquery/dist/jquery.min.js"></script>

      <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <!-- top navigation -->
        <div class="top_nav">

          <div class="nav_menu">
            <nav class="" role="navigation">

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <?php
                      echo $_SESSION['username'];
                      ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a id="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                </li>
    <li>
    <a href="judging">Grade</a>
    </li>
              </ul>
            </nav>
          </div>

        </div>
        <!-- /top navigation -->
        <div class="right_col" role="main">
          <!-- page content -->
            <div class="">
              <div class="row">
                <div class="col-lg-12">
                  <div class="page-title">
                    <div class="title_left">
                      <h1>Standings</h1>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>

              <div class="row">

                <div class="col-md-6 col-sm-6 col-sx-12 profile_details">
                  <div class="pull-right well profile_view">
                      <div class="col-md-6 col-xs-6 idMargins">
                        <img src="images/mcrofton.jpg" alt="" class="img-square img-responsive">
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="row pull-left">
                              <h2>Max Crofton</h2>
                              <p>Player: <strong>1</strong> </p>
                              <p>Nickname: <strong>Back Row Jam</strong></p>
                              <p>Hometown: <strong>Murray, KY</strong></p>
                              <p>Year in School: <strong>Junior</strong> </p> 
                              <p>School: <strong>Murray State University</strong></p> 
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-sm-4 col-xs-4">
                             
                          </div>
                          <div class="col-md-7 col-sm-8 col-xs-8">

                            <div class="pull-left">
                              <span id="left" class="chart" data-percent="86">
                                <span class="percent"></span>
                              </span>
                            </div>
                            <div class="label_easy">OVR</div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6 col-sx-12 profile_details">
                  <div class="pull-left well profile_view">

                      <div class="col-md-6 col-xs-6 idMargins">
                        <img src="images/hfarley.jpg" alt="" class="img-square img-responsive">
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="row pull-left">
                              <h2>Hunter Farley</h2>
                              <p>Player: <strong>2</strong> </p>
                              <p>Nickname: <strong>The Pedalnator</strong></p>
                              <p>Hometown: <strong>Halls, TN</strong></p>
                              <p>Year in School: <strong>Sophomore</strong> </p> 
                              <p>School: <strong>University of Memphis</strong></p> 
                        </div>
                        <div class="row">
                          <div class="col-md-5 col-sm-4 col-xs-4">
                             
                          </div>
                          <div class="col-md-7 col-sm-8 col-xs-8">
                            <div class="pull-left">
                              <span id="right" class="chart" data-percent="23">
                                <span class="percent"></span>
                              </span>
                            </div>
                            <div class="label_easy">OVR</div>
                          </div>
                        </div>
                      </div>
                      
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_content responsive">

                      <!--<div id="echart_bar_horizontal" style="height:370px;"></div>-->
                      <div id="echart_bar_horizontal" style=" min-height: 375px;">
                      <!-- was trying to make the bar graph responsive to screen size changes, but I failed. Help fix it if you can-->
                        <script>
                          $(window).on('resize', function(){
                            if(chart != null && chart != undefined){
                              chart.resize();
                             }
                          });
                        </script>
                      </div>
                      

                    </div>
                  </div>
                </div>
              </div>
            </div>
          <!-- /page content -->
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- ECharts -->
    <script src="../vendors/echarts/dist/echarts.min.js"></script>
    <script src="../vendors/echarts/map/js/world.js"></script>
    <!-- easy-pie-chart -->
    <script src="../vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>

    <script>
        var techinique="<?php echo $_SESSION['techinique']; ?>";
        var rhythmic="<?php echo $_SESSION['rhythmic']; ?>";
        var tempo="<?php echo $_SESSION['tempo']; ?>";
        var dynamics="<?php echo $_SESSION['dynamics']; ?>";
        var articulation="<?php echo $_SESSION['articulation']; ?>";
        var inotation="<?php echo $_SESSION['inotation']; ?>";
        var tone="<?php echo $_SESSION['tone']; ?>";
        var style="<?php echo $_SESSION['style']; ?>";
        var expression="<?php echo $_SESSION['expression']; ?>";
        var stagepresence="<?php echo $_SESSION['stagepresence']; ?>";
        var penalty= "<?php echo $_SESSION['penalty']; ?>";
        var techinique2="<?php echo $_SESSION['techinique2']; ?>";
        var rhythmic2="<?php echo $_SESSION['rhythmic2']; ?>";
        var tempo2="<?php echo $_SESSION['tempo2']; ?>";
        var dynamics2="<?php echo $_SESSION['dynamics2']; ?>";
        var articulation2="<?php echo $_SESSION['articulation2']; ?>";
        var inotation2="<?php echo $_SESSION['inotation2']; ?>";
        var tone2="<?php echo $_SESSION['tone2']; ?>";
        var style2="<?php echo $_SESSION['style2']; ?>";
        var expression2="<?php echo $_SESSION['expression2']; ?>";
        var stagepresence2="<?php echo $_SESSION['stagepresence2']; ?>";
        var penalty2= "<?php echo $_SESSION['penalty2']; ?>";
 
      var theme = {
          color: [
              'Red', 'Blue', '#BDC3C7', '#3498DB',
              '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
          ],

          title: {
              itemGap: 8,
              textStyle: {
                  fontWeight: 'normal',
                  color: '#408829'
              }
          },

          dataRange: {
                color: ['#1f610a', '#97b58d']
          },

          toolbox: {
              color: ['#408829', '#408829', '#408829', '#408829']
          },

          tooltip: {
              backgroundColor: 'rgba(0,0,0,0.5)',
              axisPointer: {
                  type: 'line',
                  lineStyle: {
                      color: '#408829',
                      type: 'dashed'
                  },
                  crossStyle: {
                      color: '#408829'
                  },
                  shadowStyle: {
                      color: 'rgba(200,200,200,0.3)'
                  }
              }
          },

          dataZoom: {
              dataBackgroundColor: '#eee',
              fillerColor: 'rgba(64,136,41,0.2)',
              handleColor: '#408829'
          },
          grid: {
              borderWidth: 0,
              x: 96
          },

          categoryAxis: {
              axisLine: {
                  lineStyle: {
                      color: '#408829'
                  }
              },
              splitLine: {
                  lineStyle: {
                      color: ['#eee']
                  }
              }
          },

          valueAxis: {
              axisLine: {
                  lineStyle: {
                      color: '#408829'
                  }
              },
              splitArea: {
                  show: true,
                  areaStyle: {
                      color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
                  }
              },
              splitLine: {
                  lineStyle: {
                      color: ['#eee']
                  }
              }
          },
          timeline: {
              lineStyle: {
                  color: '#408829'
              },
              controlStyle: {
                  normal: {color: '#408829'},
                  emphasis: {color: '#408829'}
              }
          },

          k: {
              itemStyle: {
                  normal: {
                      color: '#68a54a',
                      color0: '#a9cba2',
                      lineStyle: {
                          width: 1,
                          color: '#408829',
                          color0: '#86b379'
                      }
                  }
              }
          },
          map: {
              itemStyle: {
                  normal: {
                      areaStyle: {
                          color: '#ddd'
                      },
                      label: {
                          textStyle: {
                              color: '#c12e34'
                          }
                      }
                  },
                  emphasis: {
                      areaStyle: {
                          color: '#99d2dd'
                      },
                      label: {
                          textStyle: {
                              color: '#c12e34'
                          }
                      }
                  }
              }
          },
          force: {
              itemStyle: {
                  normal: {
                      linkStyle: {
                          strokeColor: '#408829'
                      }
                  }
              }
          },
          chord: {
              padding: 4,
              itemStyle: {
                  normal: {
                      lineStyle: {
                          width: 1,
                          color: 'rgba(128, 128, 128, 0.5)'
                      },
                      chordStyle: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          }
                      }
                  },
                  emphasis: {
                      lineStyle: {
                          width: 1,
                          color: 'rgba(128, 128, 128, 0.5)'
                      },
                      chordStyle: {
                          lineStyle: {
                              width: 1,
                              color: 'rgba(128, 128, 128, 0.5)'
                          }
                      }
                  }
              }
          },
          gauge: {
              startAngle: 225,
              endAngle: -45,
              axisLine: {
                  show: true,
                  lineStyle: {
                      color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                      width: 8
                  }
              },
              axisTick: {
                  splitNumber: 10,
                  length: 12,
                  lineStyle: {
                      color: 'auto'
                  }
              },
              axisLabel: {
                  textStyle: {
                      color: 'auto'
                  }
              },
              splitLine: {
                  length: 18,
                  lineStyle: {
                      color: 'auto'
                  }
              },
              pointer: {
                  length: '90%',
                  color: 'auto'
              },
              title: {
                  textStyle: {
                      color: '#333'
                  }
              },
              detail: {
                  textStyle: {
                      color: 'auto'
                  }
              }
          },
          textStyle: {
              fontFamily: 'Arial, Verdana, sans-serif'
          }
      };

      var echartBar = echarts.init(document.getElementById('echart_bar_horizontal'), theme);

      echartBar.setOption({
        
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          x: 1,
          data: ['Player 1', 'Player 2']
        },

        calculable: true,
        xAxis: [{
          type: 'value',
  min:0,
  max:20,
          boundaryGap: [0, 0.01]
        }],
        yAxis: [{
          type: 'category',
          data: ['Technique', 'Rhythm', 'Tempo', 'Dynamics', 'Articulation', 'Intonation', 'Tone', 'Style', 'Expression', 'Stage Presence'].reverse()
        }],
        series: [{
          name: 'Player 1',
          type: 'bar',
          data: [techinique, rhythmic, tempo, dynamics, articulation, inotation, tone, style,expression,stagepresence].reverse()
        }, {
          name: 'Player 2',
          type: 'bar',
          data: [techinique2, rhythmic2, tempo2, dynamics2, articulation2, inotation2, tone2, style2,expression2,stagepresence2].reverse()
        }]
      });

      $("#logout").on('click',function(){
          $.ajax({
              url: 'logout.php',
              error: function() {
                  //$('#info').html('<p>An error has occurred</p>');
                  console.log('Error');
              },
              success: function(msg) {
                  window.location.href='login';
              },
              type: 'POST'
          });
      });
 $(window).on('resize', function(){
                            if(echartBar != null && echartBar != undefined){
                              echartBar.resize();
                             }
                          });
    </script>

    <!-- easy-pie-chart1 -->
    <script>
      $(document).ready(function() {
         var ova1="<?php echo $_SESSION['ova1']; ?>";
  var ova2="<?php echo $_SESSION['ova2']; ?>";
  $("#left").attr("data-percent",ova1);
  $("#right").attr("data-percent",ova2);
        $('.chart').easyPieChart({
          easing: 'easeOutBounce',
          lineWidth: '6',
          barColor: '#0698F7',
          size: '70',
          onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          }
        });
        var chart = window.chart = $('.chart').data('easyPieChart');
        $('.js_update').on('click', function() {
          chart.update(Math.random() * 200 - 100);
        });

        //hover and retain popover when on popover content
        var originalLeave = $.fn.popover.Constructor.prototype.leave;
        $.fn.popover.Constructor.prototype.leave = function(obj) {
          var self = obj instanceof this.constructor ?
            obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
          var container, timeout;

          originalLeave.call(this, obj);

          if (obj.currentTarget) {
            container = $(obj.currentTarget).siblings('.popover');
            timeout = self.timeout;
            container.one('mouseenter', function() {
              //We entered the actual popover – call off the dogs
              clearTimeout(timeout);
              //Let's monitor popover content instead
              container.one('mouseleave', function() {
                $.fn.popover.Constructor.prototype.leave.call(self, self);
              });
            });
          }
        };

        $('body').popover({
          selector: '[data-popover]',
          trigger: 'click hover',
          delay: {
            show: 50,
            hide: 400
          }
        });
      });
    </script>
    <!-- easy-pie-chart1 -->

  </body>
</html>