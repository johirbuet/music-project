-- noinspection SqlNoDataSourceInspectionForFile
-- noinspection SqlDialectInspectionForFile
/* Create Database*/
CREATE DATABASE MUSIC;

/*USe MUSIC Database */
USE MUSIC;

/*Create Students Table*/
CREATE TABLE Students
(
StudentID INT AUTO_INCREMENT,
LastName VARCHAR(255),
FirstName VARCHAR(255),
Email VARCHAR(255),
Phone VARCHAR(255),
PRIMARY KEY (StudentID)
);

/*Create Judge Table*/
CREATE TABLE Judges
(
JudgeID INT AUTO_INCREMENT,
LastName VARCHAR(255),
FirstName VARCHAR(255),
Email VARCHAR(255),
Phone VARCHAR(255),
username VARCHAR(255),
password VARCHAR(255),
totaljudged INT,
PRIMARY KEY (JudgeID)
);

/*Create Score Table*/
CREATE TABLE Score
(
ScoreID INT AUTO_INCREMENT,
StudentID INT,
JudgeID INT,
Technique INT,
Rhythmic INT,
Tempo	INT,
Dynamics INT,
Articulation INT,
Intonation INT,
Tone INT,
Style INT,
Expression INT,
StagePresence INT,
PRIMARY KEY (ScoreID)
);

/*INSERT Dummy Students*/
INSERT INTO Students (StudentID, LastName, FirstName, Email, Phone)
VALUES (1005,'Welke','Delphine','temp@gmail.com','5110004000');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Wilber','Alane','temp2@gmail.com','5110004001');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Lugar','Barney','temp3@gmail.com','5110004002');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Napier','Jonna','temp4@gmail.com','5110004003');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Straley','Precious','temp5@gmail.com','5110004004');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Brandel','Reyes','temp6@gmail.com','5110004005');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Etheredge','Leone','temp7@gmail.com','5110004006');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Weeden','Mila','temp8@gmail.com','5110004007');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Woodbridge','Kiley','temp9@gmail.com','5110004008');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Bittner','Salley','temp10@gmail.com','5110004009');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Sol','Stephenie','temp11@gmail.com','5110004010');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Pound','Ping','temp12@gmail.com','5110004011');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Killough','Ruby','temp13@gmail.com','5110004012');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Difilippo','Valorie','temp14@gmail.com','5110004013');
INSERT INTO Students (LastName, FirstName, Email, Phone)
VALUES ('Clear','Eldon','temp15@gmail.com','5110004014');
/*INSERT Dummy Judges*/
INSERT INTO Judges (JudgeID, FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES (2005,'LastJudgeA','FirstJudgeA','jtemp@gmail.com','5110005001','username1','password1',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeB','FirstJudgeB','jtemp1@gmail.com','5110005002','username2','password2',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeB','FirstJudgeB','jtemp2@gmail.com','5110005002','username2','password2',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeC','FirstJudgeC','jtemp3@gmail.com','5110005003','username3','password3',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeD','FirstJudgeD','jtemp4@gmail.com','5110005004','username4','password4',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeE','FirstJudgeE','jtemp5@gmail.com','5110005005','username5','password5',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeF','FirstJudgeF','jtemp6@gmail.com','5110005006','username6','password6',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeG','FirstJudgeG','jtemp7@gmail.com','5110005007','username7','password7',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeH','FirstJudgeH','jtemp8@gmail.com','5110005008','username8','password8',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeI','FirstJudgeI','jtemp9@gmail.com','5110005009','username9','password9',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeJ','FirstJudgeJ','jtemp10@gmail.com','5110005010','username10','password10',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeK','FirstJudgeK','jtemp11@gmail.com','5110005011','username11','password11',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeL','FirstJudgeL','jtemp12@gmail.com','5110005012','username12','password12',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeM','FirstJudgeM','jtemp13@gmail.com','5110005013','username13','password13',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeN','FirstJudgeN','jtemp14@gmail.com','5110005014','username14','password14',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeO','FirstJudgeO','jtemp15@gmail.com','5110005015','username15','password15',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeP','FirstJudgeP','jtemp16@gmail.com','5110005016','username16','password16',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeQ','FirstJudgeQ','jtemp17@gmail.com','5110005017','username17','password17',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeR','FirstJudgeR','jtemp18@gmail.com','5110005018','username18','password18',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeS','FirstJudgeS','jtemp19@gmail.com','5110005019','username19','password19',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeT','FirstJudgeT','jtemp20@gmail.com','5110005020','username20','password20',0);
INSERT INTO Judges (FirstName,LastName, Email, Phone,username,password,totaljudged)
VALUES ('LastJudgeU','FirstJudgeU','jtemp21@gmail.com','5110005021','username21','password21',0);

/*Create Judge Table*/
CREATE TABLE Instructor
(
instructorID INT AUTO_INCREMENT,
LastName VARCHAR(255),
FirstName VARCHAR(255),
Email VARCHAR(255),
Password VARCHAR(255),
Institution VARCHAR(255),
StudioSize INT,
PRIMARY KEY (instructorID)
);

/*Alter Student Table*/

ALTER TABLE Students
ADD COLUMN classid INT,
ADD COLUMN profile VARCHAR(255),
ADD COLUMN password VARCHAR(255),
ADD COLUMN nickname VARCHAR(255),
ADD COLUMN address VARCHAR(255),
ADD COLUMN hometown VARCHAR(255),
ADD COLUMN birthday DATE,
ADD COLUMN school VARCHAR(255),
ADD COLUMN instmodel VARCHAR(255),
ADD COLUMN mouthpiecemodel VARCHAR(255),
ADD COLUMN valveoild VARCHAR(255)

/*Alter Instructor Table*/
ALTER TABLE Instructor
ADD COLUMN classID INT AFTER `instructorID`


ALTER TABLE Students
ADD COLUMN confirmation VARCHAR(255) UNIQUE