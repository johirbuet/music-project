<?php
session_start();
include "db.php";
$conf=$_GET['code'];
$_SESSION['conf']=$conf;
$query="SELECT * FROM students WHERE confirmation = :conf";
$records = $databaseConnection->prepare($query);
$records->bindParam(':conf', $conf);
$records->execute();
$results = $records->fetch(PDO::FETCH_ASSOC);
if(!(count($results)>0 && $conf==$results['confirmation']))
{
    echo("Your Link is expired");
    return;
}
$_SESSION['email']=$results['Email'];
?>
<html>
<head>

</head>
<body>
<div>
    Enter Your Password:<br>
    <input type="password" id="pwd"> <br>
    Confirm Password:<br>
    <input type="password" id="cpwd"> <br>
    <input type="submit" id="spwd" value="Set Password">
</div>
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<script>
    $("#spwd").on('click',function(){
       var email="<?php echo $_SESSION['email'];?>";
        var pwd=$("#pwd").val();
        var conf="<?php echo $_SESSION['conf'];?>";
        var data={'email':email,'pwd':pwd,'conf':conf};
        $.ajax({
            url: 'trychangestudentpwd.php',
            data: {'data':data},
            type: 'POST',
            error: function() {
                console.log('Error');
            },
            success: function(msg) {
                //console.log(msg);
                window.location.href = "studentlogin";
            }
        });
    });
</script>
</body>
</html>
