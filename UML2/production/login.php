<?php
session_start();
if(isset($_SESSION['username']))
{
  header('location:standings');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>UML | Login</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="icon" href="images/UML.jpg">

    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
  </head>

  <body style="background:#F7F7F7;">
    <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>

      <div id="wrapper">
        <div id="login" class=" form">
          <section class="login_content">
            <form>
              <h1 style="color: white">UML Login</h1>
              <div>
                <input type="text" id="user" class="form-control" style="background: white" placeholder="Username" required="" />
              </div>
              <div>
                <input id="pwd" style="background: white"  type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a id="blogin" class="btn btn-default submit">Log in</a>
              </div>
              <div class="clearfix"></div>
            </form>
          </section>
        </div>
        </div>
      </div>
    </div>
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script>
      $("#blogin").on('click',function(){
        var username=$("#user").val();
        var password=$("#pwd").val();
        console.log(username);
        console.log(password);
        var data={'username':username,'password':password};
        $.ajax({
          url: 'trylogin.php',
          data: {'data':data},
          type: 'POST',
          error: function() {
            console.log('Error');
          },
          success: function(msg) {
            //console.log(msg);
            window.location.href = "standings";
          }
        });
      });
    </script>
  </body>
</html>