<?php
session_start();
include_once 'db.php';
/*$validextensions = array("jpeg", "jpg", "png");
$temporary = explode(".", $_FILES["file"]["name"]);
$file_extension = end($temporary);
if ((($_FILES["file"]["type"] == "image/png")
        || ($_FILES["file"]["type"] == "image/jpg")
        || ($_FILES["file"]["type"] == "image/jpeg"))
    && ($_FILES["file"]["size"] < 10000000)//approx. 100kb files can be uploaded
    && in_array($file_extension, $validextentions)){
    if (file_exists("studentimg/" . $_FILES["file"]["name"])) {
        echo $_FILES["file"]["name"] . " <b>already exists.</b> ";
    }
    else {
        move_uploaded_file($_FILES["file"]["tmp_name"],
            "studentimg/" . $_FILES["file"]["name"]);
        echo "<b>Stored in:</b> " . "studentimg/" . $_FILES["file"]["name"];
        $url="studentimg/" . $_FILES["file"]["name"];
        $_SESSION['profile']=$url;
        $email=$_SESSION['student'];
        $sql="UPDATE `students` SET `profile` = :url WHERE `Email` = :email";
        $records2 = $databaseConnection->prepare($sql);
        $records2->bindParam(':url',$url);
        $records2->bindParam(':email',$email);
        $records->execute();
    }
}*/

if(isset($_POST['submit'])){
    $name       = $_FILES['file']['name'];
    $temp_name  = $_FILES['file']['tmp_name'];
    if(isset($name)){
        if(!empty($name)){
            $location = 'studentimg/';
            if(move_uploaded_file($temp_name, $location.$name)){
                $url="studentimg/" . $_FILES["file"]["name"];
                $_SESSION['profile']=$url;
                $email=$_SESSION['student'];
                $sql="UPDATE `students` SET `profile` = :url WHERE `Email` = :email";
                $records2 = $databaseConnection->prepare($sql);
                $records2->bindParam(':url',$url);
                $records2->bindParam(':email',$email);
                $records2->execute();
                echo 'File uploaded successfully';
                header('Location: studentProfile');
            }
        }
    }  else {
        echo 'You should select a file to upload !!';
    }
}