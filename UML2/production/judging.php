<?php
session_start();
if(!isset($_SESSION['username']))
{
  header('location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>UML | Judging</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Tab icon -->
    <link rel="icon" href="images/UML.jpg">
    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
  </head>


  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        

        <!-- top navigation -->
        <div class="top_nav">

          <div class="nav_menu">
            <nav class="" role="navigation">

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php
                    echo $_SESSION['username'];
                    ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a id="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>JUDGING FORM</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>SCORE SUBMISSION</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                               Step 1<br/>
                              <small>Submit Player 1 score</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                              Step 2<br />
                              <small>Submit Player 2 score</small>
                            </span>
                          </a>
                        </li>
                      </ul>
                      <div id="step-1">
                         <form class="form-horizontal form-label-left">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="technique" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Technique</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="rhythmic" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Rhythm</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="tempo" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Tempo</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="dynamics" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Dynamics</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="articulation" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Articulation</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="intonation" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Intonation</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="tone" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Tone</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="style" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Style</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="expression" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Musicality</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="stagepresence" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Stage Presence</label>
                            </div>
                          </div>
                          
                        </form>
                      </div>
                      <div id="step-2">
                        <form class="form-horizontal form-label-left">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="technique2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Technique</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="rhythmic2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Rhythm</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="tempo2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Tempo</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="dynamics2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Dynamics</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="articulation2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Articulation</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="intonation2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Intonation</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="tone2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Tone</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="style2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Style</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="expression2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Musicality</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-2 col-sm-4 col-xs-4 ">
                                <select id="stagepresence2" class="form-control">
                                  <option>0</option>
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                  <option>6</option>
                                  <option>7</option>
                                  <option>8</option>
                                  <option>9</option>
                                  <option>10</option>
                                  <option>11</option>
                                  <option>12</option>
                                  <option>13</option>
                                  <option>14</option>
                                  <option>15</option>
                                  <option>16</option>
                                  <option>17</option>
                                  <option>18</option>
                                  <option>19</option>
                                  <option>20</option>
                                </select>
                              </div>
                              <label class="control-label col-md-10 col-sm-8 col-xs-8 text-left">Stage Presence</label>
                            </div>
                          </div>
                          
                            
                        </form>
                      </div>

                    </div>
                    <!-- End SmartWizard Content -->
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="x_panel judging_Ex">
                  <div class="x_title">
                    <h2>SCORING EXPLANATIONS </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content x_explain">

                    <div class="col-lg-4 col-md-5 col-sm-6 col-xs-5">
                      <!-- required for floating -->
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs tabs-left">
                        <li class="active"><a href="#ptechnique" data-toggle="tab">Technique</a>
                        <li><a href="#prhythmic" data-toggle="tab">Rhythm</a>
                        <li><a href="#ptempo" data-toggle="tab">Tempo</a>
                        <li><a href="#pdynamics" data-toggle="tab">Dynamics</a>
                        <li><a href="#particulation" data-toggle="tab">Articulation</a>
                        <li><a href="#pintonation" data-toggle="tab">Intonation</a>
                        <li><a href="#ptone" data-toggle="tab">Tone</a>
                        <li><a href="#pstyle" data-toggle="tab">Style</a>
                        <li><a href="#pstage_Presence" data-toggle="tab">Stage Presence</a>
                        <li><a href="#pmusicality" data-toggle="tab">Musicality</a></li>
                      </ul>
                    </div>

                    <div class="col-lg-8 col-md-7 col-sm-6 col-xs-7">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="ptechnique">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ A brave attempt with rather too many major errors</p>
                          <p>- Very noticeable and constant errors</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ Fine in most technical details but some quite frequent noticeable small errors</p>
                          <p>- Mostly clean but too frequent errors rather spoil the performance</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Virtually all details totatlly correct. Nothing major of concern</p>
                          <p>- A generally clean performance with one or two quite noticeable errors that do detract</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Totally accurate with no discernable errors. All details audible and executed to perfection</p>
                          <p>- An outstandingly accurate performance with only the tiniest os slips</p>
                        </div>
                        <div class="tab-pane" id="prhythmic">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Some quite noticeable rhythmic insecurities</p>
                          <p>- Disappointing attention to rhythmic detail</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ One or two rhythmic sections not played with enough clarity, but generally fine</p>
                          <p>- Whilst the impression is positive some lack of rhythmic detail detracts</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Virtually all details totally correct. Nothing major of concern</p>
                          <p>- Some slight cause for concern and possible lapses in clarity</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Outstanding rhythmic playing, executed with total conviction from start to finish</p>
                          <p>- A full grasp of all details executed with style and verve</p>
                        </div>
                        <div class="tab-pane" id="ptempo">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Rather erratic tempi and composers intentions often disregarded</p>
                          <p>- Rather poor tempi choice with seriously hinders the performance</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ Good observance of detail but perhaps chosen tempi cause loss of flow or clarity</p>
                          <p>- Mostly sensible tempi that make musical sense, but with some more questionable moments</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Tempi are faithful to the composer’s wishes and all inter- relations and transitions well judged</p>
                          <p>- Most tempi seem to be totally correct with only the odd moment of concern</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Complete coherent and masterful choice of tempi, making total musical sense</p>
                          <p>- Totally assured and confident reading without any misjudgements</p>
                        </div>
                        <div class="tab-pane" id="pdynamics">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Use of dynamics is disappointing and extremes often not controlled or musical</p>
                          <p>- Frequent over blowing and/or disregard for the composers intentions</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ Composer’s markings faithfully attempted throughout, but some noticeable rough or wild moments</p>
                          <p>- Good choice of dynamics but with either lack of contrasts or uncontrolled moments detracting</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ A very good use of dynamic contracts with no serious lapses in detail or quality</p>
                          <p>- Impressive use of dynamics with only occasional over blowing or loss of control</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Outstandingly well controlled large dynamic range that shows the mark of true quality</p>
                          <p>- Very impressive use of dynamics that seems totally appropriate for the piece</p>
                        </div>
                        <div class="tab-pane" id="particulation">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Articulation precision needs some serious attention as frequent lapses cause concern</p>
                          <p>- Poor clarity of articulations throughout</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ Most articulations played with precision, but some poor moments do concern</p>
                          <p>- Several sections of concern where the clarity of articulations waver</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Generally very good precision with all sections of the piece having clear articulation</p>
                          <p>- Articulation precision is of a high level but one or two moments detract from an otherwise very good showing</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Immaculate clarity and ease of articulations</p>
                          <p>- Highly impressive clarity of articulation</p>
                        </div>
                        <div class="tab-pane" id="pintonation">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Frequent tuning issues are a major disappointment in the performance</p>
                          <p>- Very disappointing lack of attention to this facet of the performance</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ A high level of general tuning but with frequent lapses in exposed sections or extremes of dynamics</p>
                          <p>- Tuning is a negative factor in the appreciation of the performance</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Generally no areas for concern save for the odd moment</p>
                          <p>- Very good intonation but marred by a few poor moments that detract</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Totally convincing with little or no areas of tuning problems whatsoever</p>
                          <p>- Very impressive playing from start to finish</p>
                        </div>
                        <div class="tab-pane" id="ptone">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Sound is not one of the strengths of the performance, perhaps harsh or thin sounds dominate</p>
                          <p>- Not a beautiful sound, with lack of blend, warmth and quality</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ A good sounding performance but a few features just detract from the overall impression</p>
                          <p>- A sound that lacks a little quality but still maintains a consistently good level</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ An impressive sound with rich tones and lyrical beauty</p>
                          <p>- Very pleasing sound with only a few colors missing</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Tremendous sound, enhanced by total control, balance and a sublime quality</p>
                          <p>- Outstanding sound quality which leaves a lasting impression</p>
                        </div>
                        <div class="tab-pane" id="pstyle">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Not enough consideration to composer intent or overall style of the performance</p>
                          <p>- Disappointing interpretation of style</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ A generally good approach to style with sections of the performance lacking convincing interpretation</p>
                          <p>- Some noticeable and frequent lapses in stylistic interpretation</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Very good approach to style. Occasional lapses in the approach that are quickly brought back into focus</p>
                          <p>- Good approach and understanding with some performer inaccuracies hindering the overall audience impression</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Total mastery of style throughout the performance that is true to the composers intent and voice</p>
                          <p>- Outstanding style exhibited with very few gaps in performer’s exhibition</p>
                        </div>
                        <div class="tab-pane" id="pstage_Presence">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ Either nerves or fragility seem to be a factor here</p>
                          <p>- Not the best day for the performer</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ Fine playing but some insecure moments have a slightly negative effect on the performance</p>
                          <p>- Some very fine moments but too many uncomfortable moments for the audience</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Very fine performer who delivers with confidence and accuracy. Just the odd uncomfortable and noticeable moments</p>
                          <p>- Generally acceptable presence and audience comfortability with a fine performance</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ Performer performs with style, accuracy and beauty of tone, delivered with a sense of complete mastery. Presence is comfortable on stage, with superb audience connection appropriate for the venue</p>
                          <p>- Outstanding playing with only the slightest amount of uncomfortability on stage</p>
                        </div>
                        <div class="tab-pane" id="pmusicality">
                          <p class="lead">Fair : <small>1 - 5 range</small></p>
                          <p>+ The performer never seemed really comfortable with the piece to try anything more than the notes</p>
                          <p>- Lack of persuasive musical moments was something of a disappointment here</p>

                          <p class="lead">Good : <small>6 - 10 range</small></p>
                          <p>+ Whilst a good performance, some musicality and artistry wasn’t able to communicate itself on the day</p>
                          <p>- Some nice musical moments but the overall impression was not very persuasive</p>

                          <p class="lead">Very Good : <small>11 - 15 range</small></p>
                          <p>+ Very good artistry shown, with all stylistic elements clearly realised with some really fine moments</p>
                          <p>- Overall a really fine reading with some captivating moments</p>

                          <p class="lead">Outstanding : <small>16 - 20 range</small></p>
                          <p>+ A totally convincing reading with every facet of the piece impressing. Moments of sublime beauty and music making of the utmost quality</p>
                          <p>- Outstanding and impressive performance that really told a story and had moments of magic that transcended the notes</p>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /page content -->

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>

    <!-- jQuery Smart Wizard -->
    <script>
      $(document).ready(function() {
        $('#wizard').smartWizard({onFinish:finish});

        $('#wizard_verticle').smartWizard({transitionEffect:'slide'});
        function onFinishCallback(){
          $('#wizard').smartWizard('showMessage','Finish Clicked');
          alert('Finish Clicked');
        }

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
      });
      $("#wizard").on('click','#gsubmit',function(){
        console.log("Hello Johir");
      });
      $(".buttonFinish").on('click',function(event){
        console.log("I am Clicked");
        event.preventDefault();
        var techinique=$("#technique option:selected").text();
        var rhythmic=$("#rhythmic option:selected").text();
        var tempo=$("#tempo option:selected").text();
        var dynamics=$("#dynamics option:selected").text();
        var articulation=$("#articulation option:selected").text();
        var inotation=$("#intonation option:selected").text();
        var tone=$("#tone option:selected").text();
        var style=$("#style option:selected").text();
        var expression=$("#expression option:selected").text();
        var stagepresence=$("#stagepresence option:selected").text();
        var penalty=$("#penalty option:selected").text();
        var techinique2=$("#technique2 option:selected").text();
        var rhythmic2=$("#rhythmic2 option:selected").text();
        var tempo2=$("#tempo2 option:selected").text();
        var dynamics2=$("#dynamics2 option:selected").text();
        var articulation2=$("#articulation2 option:selected").text();
        var inotation2=$("#intonation2 option:selected").text();
        var tone2=$("#tone2 option:selected").text();
        var style2=$("#style2 option:selected").text();
        var expression2=$("#expression2 option:selected").text();
        var stagepresence2=$("#stagepresence2 option:selected").text();
        var penalty2=$("#penalty2 option:selected").text();
        var data={'penalty':penalty,'techinique':techinique,'rhythmic':rhythmic,'tempo':tempo,'dynamics':dynamics,
          'articulation':articulation,'inotation':inotation,'tone':tone,'style':style,'expression':expression,
          'stagepresence':stagepresence,'penalty2':penalty2,'techinique2':techinique2,'rhythmic2':rhythmic,'tempo2':tempo2,'dynamics2':dynamics2,
          'articulation2':articulation2,'inotation2':inotation2,'tone2':tone2,'style2':style2,'expression2':expression2,
          'stagepresence2':stagepresence2};
        $.ajax({
          url: 'submitgrade.php',
          data: {'data':data},
          error: function() {
            //$('#info').html('<p>An error has occurred</p>');
            console.log('Error');
          },
          success: function(msg) {
            window.location.href='standings.php';
          },
          type: 'POST'
        });
      });

      $("#logout").on('click',function(){
        $.ajax({
          url: 'logout.php',
          error: function() {
            //$('#info').html('<p>An error has occurred</p>');
            console.log('Error');
          },
          success: function(msg) {
            window.location.href='login';
          },
          type: 'POST'
        });
      });

      var finish=function(){
        event.preventDefault();
        var techinique=$("#technique option:selected").text();
        var rhythmic=$("#rhythmic option:selected").text();
        var tempo=$("#tempo option:selected").text();
        var dynamics=$("#dynamics option:selected").text();
        var articulation=$("#articulation option:selected").text();
        var inotation=$("#intonation option:selected").text();
        var tone=$("#tone option:selected").text();
        var style=$("#style option:selected").text();
        var expression=$("#expression option:selected").text();
        var stagepresence=$("#stagepresence option:selected").text();
        var penalty=$("#penalty option:selected").text();
        var techinique2=$("#technique2 option:selected").text();
        var rhythmic2=$("#rhythmic2 option:selected").text();
        var tempo2=$("#tempo2 option:selected").text();
        var dynamics2=$("#dynamics2 option:selected").text();
        var articulation2=$("#articulation2 option:selected").text();
        var inotation2=$("#intonation2 option:selected").text();
        var tone2=$("#tone2 option:selected").text();
        var style2=$("#style2 option:selected").text();
        var expression2=$("#expression2 option:selected").text();
        var stagepresence2=$("#stagepresence2 option:selected").text();
        var penalty2=$("#penalty2 option:selected").text();
        var data={'penalty':penalty,'techinique':techinique,'rhythmic':rhythmic,'tempo':tempo,'dynamics':dynamics,
          'articulation':articulation,'inotation':inotation,'tone':tone,'style':style,'expression':expression,
          'stagepresence':stagepresence,'penalty2':penalty2,'techinique2':techinique2,'rhythmic2':rhythmic,'tempo2':tempo2,'dynamics2':dynamics2,
          'articulation2':articulation2,'inotation2':inotation2,'tone2':tone2,'style2':style2,'expression2':expression2,
          'stagepresence2':stagepresence2};
        $.ajax({
          url: 'submitgrade.php',
          data: {'data':data},
          error: function() {
            //$('#info').html('<p>An error has occurred</p>');
            console.log('Error');
          },
          success: function(msg) {
            //console.log(msg);
           window.location.href='standings';
          },
          type: 'POST'
        });
      }
    </script>
    <!-- /jQuery Smart Wizard -->
  </body>
</html>