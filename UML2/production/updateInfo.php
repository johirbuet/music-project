<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>UML | Update Info </title>

    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>

    <!-- jQuery -->
    <!--<script src="../vendors/jquery/dist/jquery.min.js"></script>-->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">

    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet">
    <script src="../vendors/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">



    <!-- x-editable (bootstrap 3) -->
    <link href="../vendors/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
    <script src="../vendors/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>

    <!-- momentjs -->
    <script src="../vendors/momentjs/moment.min.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- select2 -->
    <link href="../vendors/select2/select2.css" rel="stylesheet">
    <script src="../vendors/select2/select2.js"></script>
    <link href="../vendors/select2/select2-bootstrap.css" rel="stylesheet">

    <!-- typeaheadjs -->
    <link href="../vendors/x-editable/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css" rel="stylesheet">
    <script src="../vendors/x-editable/inputs-ext/typeaheadjs/lib/typeahead.js"></script>
    <script src="../vendors/x-editable/inputs-ext/typeaheadjs/typeaheadjs.js"></script>

    <!-- demo -->
    <link href="../vendors/demo-bs3.css" rel="stylesheet">
    <script src="../vendors/demo.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>

    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">

    <style type="text/css">
        #comments:hover {
            background-color: #FFFFC0;
            cursor: text;
        }
    </style>

    <!-- address input -->
    <link href="../vendors/x-editable/inputs-ext/address/address.css" rel="stylesheet">
    <script src="../vendors/x-editable/inputs-ext/address/address.js"></script>

    <style type="text/css">
        table.table > tbody > tr > td {
            height: 30px;
            vertical-align: middle;
        }
    </style>

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="images/img.png" alt="">John Doe
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="javascript:;">  Profile</a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">Help</a>
                                </li>
                                <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="page-title">
                    <div class="title_left">
                        <h3>Update Information</h3>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <table id="user" class="table table-bordered table-striped" style="clear: both">
                                    <tbody>
                                    <tr>
                                        <td width="35%">Nickname</td>
                                        <td width="65%"><a href="#" id="username" data-type="text" data-pk="1" data-value="unknown" data-title="Edit Nickname">Unknown</a></td>
                                    </tr>
                                    <tr>
                                        <td>Birthday</td>
                                        <td><a href="#" id="dob" data-type="combodate" data-value="" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="1"  data-title="Select Date of birth"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Current Address</td>
                                        <td><a href="#" id="address" data-type="address" data-pk="1" data-title="Please, fill address"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Hometown</td>
                                        <td><a href="#" id="hometown" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Enter your hometwon">Unknown</a></td>
                                    </tr>
                                    <tr>
                                        <!--copied from "firstname id"-->
                                        <td>Current School</td>
                                        <td><a href="#" id="school" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Enter your School">Unknown</a></td>
                                    </tr>
                                    <tr>
                                        <!--copied from "firstname id"-->
                                        <td>Instrument Make/Model</td>
                                        <td><a href="#" id="instrument" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Enter your Instrument">Unknown</a></td>
                                    </tr>
                                    <tr>
                                        <!--copied from "firstname id"-->
                                        <td>Mouthpiece Make Model</td>
                                        <td><a href="#" id="monthpiece" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Enter Mouthpiece">Unknown</a></td>
                                    </tr>
                                    <tr>
                                        <!--copied from "firstname id"-->
                                        <td>Valve Oil</td>
                                        <td><a href="#" id="valveOil" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Enter your Valve Oil">Unknown</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page content -->


    </div>
</div>



<!-- bootstrap-daterangepicker -->
<script>
    $(document).ready(function() {
        $("#username").
        $('#birthday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>
<!-- /bootstrap-daterangepicker -->
</body>
</html>